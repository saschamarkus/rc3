import locale
import json

from collections import defaultdict
from datetime import datetime, timedelta

import requests

from bs4 import BeautifulSoup as bs
from icalendar import Calendar, Event

FAHRPLAN_URL = "https://rc3.world/rc3/public_fahrplan"

# set locale to get "my" locale format
locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')
headers = {"Accept-Language": "de,de-DE,en;q=0.5"}
html = requests.get(FAHRPLAN_URL, headers=headers)

soup = bs(html.text, "html5lib")

fahrplan = soup.find_all("div", class_="rc3-fahrplan text-white")

cal = Calendar()

for s in fahrplan[0].find_all("script"):
    d = json.loads(s.string)
    start = datetime.strptime(d["schedule_start"], "%d. %B %Y %H:%M")
    d["start"] = start
    duration = d["schedule_duration"].split(":")
    end = start + timedelta(hours=int(duration[0]), minutes=int(duration[1]))

    event = Event()

    event.add('summary', d['title'])
    event.add('dtstart', start)
    event.add('dtend', end)
    event.add('description', f"{d['description_html']}\nTrack:\t{d['track_name']}\nLanguage:\t{d['language']}\nSpeaker:\n{d['speakers']}")
    event.add('location', d['room_name'])
    cal.add_component(event)

with open('fahrplan.ics', 'wb') as f:
    f.write(cal.to_ical())
    f.close()
